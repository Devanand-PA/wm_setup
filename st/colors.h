
static const char *colorname[] = {
"#0c090a",      //  "black",
"#313C47",      //  "red3",
"#5B4E52",      //  "green3",
"#6B5252",      //  "yellow3",
"#9F6C50",      //  "blue2",
"#A86146",      //  "magenta3",
"#C8946B",      //  "cyan3",
"#eadac8",      //  "#444444",
"#a3988c",      //  "gray50",
"#313C47",      //  "red",                        
"#5B4E52",     //  "green",
"#6B5252",     //  "yellow",
"#9F6C50",     //  "#5c5cff" "light blue"
"#A86146",     //  "magenta"
"#C8946B",     //  "cyan",
"#eadac8",     //  "white",
                            
[255] = 0,

/* more colors can be added after 255 to use with DefaultXX */
"#444444",
"#555555",
"#eadac8",
"#0c090a", /* default background color */
};
/* bg opacity */
float alpha = 0.75;
