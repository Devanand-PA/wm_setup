
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#eadac8", "#0c090a" },
	[SchemeSel] = {  "#0c090a", "#313C47" },
	[SchemeOut] = {  "#eadac8", "#C8946B" },
};
